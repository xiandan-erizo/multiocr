FROM xiandan/ocr:latest

EXPOSE 8000

# 设置当前目录为工作目录
WORKDIR /app

COPY . /app

CMD ["python3", "./main.py"]
# CMD ["uvicorn", "main:app", "--host", "0.0.0.0"]
