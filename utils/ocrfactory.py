
from routers.easy import EasyOcr
from routers.pdocr import PdOcr
from routers.base_ocr import BaseOcr


class OcrFactory:
    def __init__(self):
        print("ocr factory init")
        self.ocr_dict = {}

    def register(self, ocr: BaseOcr):
        ocr_type = ocr.name
        print(f"ocr factory register: {ocr.name}")
        self.ocr_dict[ocr_type] = ocr

    def get(self, ocr_type: str):
        ocr = self.ocr_dict.get(ocr_type)
        if ocr is None:
            raise Exception("ocr type not found")
        return ocr


Ocrf = OcrFactory()
Ocrf.register(EasyOcr())
Ocrf.register(PdOcr())
