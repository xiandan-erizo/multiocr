from contextlib import asynccontextmanager
import os
import easyocr
from routers.base_ocr import BaseOcr
import numpy as np


class EasyOcr(BaseOcr):
    name = "EasyOcr"
    description = "easyocr"

    def __init__(self):
        use_gpu = os.getenv("GPU",False)
        print(use_gpu)
        self.reader = easyocr.Reader(['en','th'], gpu=False,model_storage_directory=".EasyOCR/")

    def perform_ocr(self, image_path: str):
        result = self.reader.readtext(image_path)
        return result
    
    def perform_result(self, image_path: str):
        res = self.reader.readtext(image_path)
        result = ""
        for r in res:
            print(r[-2])
            result+=r[-2] +"\n"
        return result