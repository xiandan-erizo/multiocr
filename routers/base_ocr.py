class BaseOcr:
    name: str
    description: str

    def __init__(self):
        pass

    def perform_ocr(self, image_path: str):
        pass
    
    def perform_result(self,image_path:str):
        pass
