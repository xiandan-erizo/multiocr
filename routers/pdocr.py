# from utils.ocrfactory import Ocrf
from paddleocr import PaddleOCR
from routers.base_ocr import BaseOcr


class PdOcr(BaseOcr):
    name = "PdOcr"
    description = "paddleocr"

    def __init__(self):
        self.ocr = PaddleOCR(use_angle_cls=True, lang="ch")

    def perform_ocr(self, image_path: str):
        return self.ocr.ocr(image_path)

    
    def perform_result(self, image_path: str):
        res = self.ocr.ocr(image_path)
        result = ""
        for r in res[0]:
            print(r[-1][0])
            # 处理结果
            result += r[-1][0] + "\n"
        return result
    